# Satzung

Dies ist die Satzung zur Gründung und dem Erhalt des Hackwerk e.V.

## Building

Die Satzung wird automatisch bei Änderungen neu gebaut und unter folgenden URLs zur Verfügung gestellt:


[Satzung](https://gitlab.com/api/v4/projects/46821041/jobs/artifacts/main/raw/satzung.pdf?job=build_satzung)

[Geschäftsordnung](https://gitlab.com/api/v4/projects/46821041/jobs/artifacts/main/raw/geschaeftsordnung.pdf?job=build_geschaeftsordnung)

